package kmodule

{
	domain: "vera.cloud"
	module: "ccproject"
	version: [
		0,
		0,
		1,
	]
	cue: "v0.4.3"
	spec: [
		1,
		0,
	]
	dependencies: {
		"kumori.systems/kumori": {
			query:  "1.1.0"
			target: "kumori.systems/kumori/@1.1.0"
		}
		"kumori.systems/builtins/inbound": {
			query:  "1.2.0"
			target: "kumori.systems/builtins/inbound/@1.2.0"
		}
	}
	sums: {
		"kumori.systems/kumori/@1.1.0":           "jnZtnOpPOBpDk5GmxCU4joiI0dYO1jfXiiYU6T4bCiA="
		"kumori.systems/builtins/inbound/@1.2.0": "W0sZkkRTtVJDw0UHDMR1Qs3GZd7EyCmgvFy1ylnTpoU="
	}
	artifacts: {}
}
