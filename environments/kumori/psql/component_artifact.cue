
package component

import (
  k "kumori.systems/kumori:kumori"
)

#Artifact: {
  ref: name: "psql"
  description: {
    srv: {
      // Listener ports. One for producers/consumers, another for internal broker comms
      server: main: port: 5432
    }

    config: {

      parameter: {
        dbname  : string
        username: string
        password: string  // NOTE: only valid for internal controlled access
      }

      resource : store    : k.#Volume
    }

		let _cfgp = config.parameter

    size: {
      bandwidth: {
        size: 1000
        unit: "M"
      }
    }
    code: postgresql: {
      name: "postgresql"
      image: {
        tag: "docker.io/postgres:13"
      }

      user: {
        userid : 0  // set the right value for this
        groupid: 0
      }

      mapping: {
        // Map a file or volume
        filesystem: {
          "/var/lib/pgsql/data": volume: "store"
        }
        env: {          
          POSTGRES_DB       : value : _cfgp.dbname
          POSTGRES_USER     : value : _cfgp.username
          POSTGRES_PASSWORD : value : _cfgp.password
          TZ                : value : "Europe/Madrid"
        }
      }

      size: {
        memory: {size: 1, unit: "G"}
        cpu:    {size: 1000, unit: "m"}
        mincpu: 1000
      }

    }
  }
}
