package component

import ( 
	k "kumori.systems/kumori:kumori"
)

// DBTypes maps the kinds of database to a URL prefix
// TODO: verify the mapping for eveerything other than postgres
#DBtypes: {
	mariadb : "mariadb"
	mssql   : "mssql"
	mysql   : "mysql"
	oracle  : "oracle"
	postgres: "postgresql"
}

#Artifact: {
	ref: name: "keycloak"

	description: {

		srv: {
			server: restapi: {
				protocol: "http"
				port    : 8080
			}
			client: dbcli  : protocol: "tcp"
		}

		config: {
			parameter: {
				masterUsername: *"admin" | string
				dbType        : *"dev-file" | "mariadb" | "mssql" | "mysql" | "oracle" | "postgres"
			}

			if parameter.dbType != "dev-file" {
				parameter: dbName    : string
				parameter: dbUsername: string
				parameter: dbPassword: string
			}

			resource: masterPassword: k.#Secret
		}

		let _cfgp = config.parameter
		
		size: bandwidth: {size: 100, unit: "M"}

		probe: keycloak: {
			liveness: {
				protocol: http: {port: srv.server.restapi.port, path: "/health/live"}
				startupGraceWindow: {unit: "ms", duration: 300000, probe: true}
				frequency: "medium"
				timeout:   20000 // msec
			}
			readiness: {
				protocol: http: {port: srv.server.restapi.port, path: "/health/ready"}
				frequency: "medium"
				timeout:   20000 // msec
			}
		}

		code: {
			keycloak: {
				name: "keycloak"

				image: {
					hub: {name: "quay.io", secret: ""}
					tag: "keycloak/keycloak:20.0"
				}

				_common_cmd: [ "start-dev",
					"--auto-build",
					"--import-realm",
					"--proxy", "edge",
					"--http-enabled", "true",
					"--hostname-strict", "false",
					"--hostname-strict-https", "false",
					"--hostname-strict-backchannel", "false"
				]
				_more_cmd: _

				cmd: _common_cmd + _more_cmd

				if _cfgp.dbType != "dev-file" {
					_more_cmd: [
						"--db", _cfgp.dbType,
						"--db-url-host", "0.dbcli",
						"--db-url-port", "80",
						"--db-url-database", _cfgp.dbName,
						"--db-username", _cfgp.dbUsername,
						"--db-password", _cfgp.dbPassword,
					]
				}

				if _cfgp.dbType == "dev-file" {
					_more_cmd: []
				}

				mapping: {
					env: {
						// Keycloak master user credentials
						KEYCLOAK_ADMIN         : value: _cfgp.masterUsername
						KEYCLOAK_ADMIN_PASSWORD: secret: "masterPassword"
						KC_HEALTH_ENABLED : value: "true"
						KC_METRICS_ENABLED: value: "true"
						KC_DB_POOL_INITIAL_SIZE: value: "1"
						KC_DB_POOL_MAX_SIZE: value: "5"
						
					}
				}
				size: {
					memory: {size: 1, unit: "G"}
					mincpu: 100
					cpu: {size: 1000, unit: "m"}
				}
			}
		}
	}
}
